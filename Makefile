.POSIX:
.SUFFIXES:
CC       = cc
BIN      = 2048
SRC      = 2048.c
INC      = 
CFLAGS   = -g -Wall
LIBFLAGS = `pkg-config --libs --cflags ncurses`
PREFIX   = /usr/local

OBJ = $(SRC:.c=.o)

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) -o $@ $^ $(LIBFLAGS) $(CFLAGS)

.SUFFIXES: .c .o
.c.o: $(INC)
	$(CC) -c -o $@ $^ $(LIBFLAGS) $(CFLAGS)

.PHONY: clean install
clean:
	rm -f $(BIN) $(OBJ)

install: all
	strip $(BIN)
	install -d $(DESTDIR)$(PREFIX)/bin
	install -m 755 $(BIN) $(DESTDIR)$(PREFIX)/bin
