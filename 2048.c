/*
 * 2048 - a command line version of 2048
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define SIZE 4

// color indexes
#define RED 1
#define GREEN 2
#define YELLOW 3
#define BLUE 4
#define MAGENTA 5
#define CYAN 6

// score counter
unsigned long score = 0;

// two dimensional array representing tiles
short board[SIZE][SIZE] = {
	{ 0, 0, 0, 0 },
	{ 0, 0, 0, 0 },
	{ 0, 0, 0, 0 },
	{ 0, 0, 0, 0 }
};

// helps keep track of merges to prevent double merging
char merges[SIZE][SIZE] = {
	{ 0, 0, 0, 0 },
	{ 0, 0, 0, 0 },
	{ 0, 0, 0, 0 },
	{ 0, 0, 0, 0 }
};

enum direction {
	LEFT,
	RIGHT,
	UP,
	DOWN
};

/*
 * some static infomration that the game logic
 * uses for directions
 */
static const int dirdat[][8] = {
	/* horiz, xi, yi, max,     sx,       sy,       ex,   ey */
	{ true,  -1, 0,  0,        0,        0,        SIZE, SIZE }, // LEFT
	{ true,  1,  0,  SIZE - 1, SIZE - 1, 0,        -1,   SIZE }, // RIGHT
	{ false, 0,  -1, 0,        0,        0,        SIZE, SIZE }, // UP
	{ false, 0,  1,  SIZE - 1, 0,        SIZE - 1, SIZE, -1   }  // DOWN
};

#define HORIZ(dir) dirdat[dir][0]
#define XI(dir) dirdat[dir][1]
#define YI(dir) dirdat[dir][2]
#define MAX(dir) dirdat[dir][3]
#define SX(dir) dirdat[dir][4]
#define SY(dir) dirdat[dir][5]
#define EX(dir) dirdat[dir][6]
#define EY(dir) dirdat[dir][7]

#define BOARD_FOREACH(x, y)				\
	for (y = 0; y < SIZE; y++)			\
		for (x = 0; x < SIZE; x++)

#define KEYTODIR(k1, k2, dir)           \
	case k1:                            \
	case k2:                            \
		if (canmove(dir))               \
			boardmove(dir);             \
		else                            \
		    return false;               \
	    break;

// function declarations
bool canmove(enum direction dir);
bool chklose();
bool chkwin();
bool userinput();
int emptycnt();
int getcolor(short block);
int cntdigits(int i);
int rngl(int l);
void boardmove(enum direction dir);
void chksize();
void exithdl();
void initcolor();
void lose();
void populate();
void win();

// gets number with maximum l
int rngl(int l)
{
	int d = RAND_MAX / l;
	int r;

	do r = rand() / d; while (r >= l);

	return r;
}

// count digits in a number
int cntdigits(int i)
{
	if (i == 0)
		return 1;

	int digits;
	for (digits = 0; i >= 1;) {
		i /= 10;
		digits++;
	}

	return digits;
}

// counts number of empty cells in board
int emptycnt()
{
	int e = 0;
	int x, y;

	BOARD_FOREACH(x, y)
		if (board[y][x] == 0)
			e++;

	return e;
}

// put value in an empty cell
void populate()
{
	int cells, nval, cellnum, x, y;
	int i = 0;

	if ((cells = emptycnt())) {
		nval = rngl(10) < 9 ? 2 : 4;
		cellnum = rngl(cells);

		BOARD_FOREACH(x, y) {
			if (board[y][x] == 0) {
				if (i == cellnum) {
					board[y][x] = nval;
					return;
				} else {
					i++;
				}
			}
		}
	}
}

// checks if any moves are available in a direction
bool canmove(enum direction dir)
{
	int x, y;

	BOARD_FOREACH(x, y) {
		if ((HORIZ(dir) ? x : y) == MAX(dir) || board[y][x] == 0)
			continue;

		if (board[y + YI(dir)][x + XI(dir)] == 0
			|| (board[y + YI(dir)][x + XI(dir)] == board[y][x]
				&& merges[y][x] == 0))
			return true;
	}

	return false;
}

// move the board in a direction
void boardmove(enum direction dir)
{
	int x, y;

	while (canmove(dir)) {
		for (y = SY(dir); y != EY(dir); y += (EY(dir) < 0 ? -1 : 1)) {
			for(x = SX(dir); x != EX(dir); x += (EX(dir) < 0 ? -1 : 1)) {
				if ((HORIZ(dir) ? x : y) == MAX(dir) || board[y][x] == 0)
					continue;

				if (board[y + YI(dir)][x + XI(dir)] == 0) {
					// move to empty space
					board[y + YI(dir)][x + XI(dir)] = board[y][x];
					board[y][x] = 0;

					// preserve merge status
					if (merges[y][x] == 1) {
						merges[y + YI(dir)][x + XI(dir)] = 1;
						merges[y][x] = 0;
					}
				} else if (board[y + YI(dir)][x + XI(dir)] == board[y][x]
						&& merges[y][x] == 0) {
					// merge with the next space
					board[y + YI(dir)][x + XI(dir)] *= 2;
					merges[y + YI(dir)][x + XI(dir)] = 1;
					board[y][x] = 0;

					// increment the score counter
					score += board[y + YI(dir)][x + XI(dir)];
				}
			}
		}
	}
}

// checks if the lose condition is met
bool chklose()
{
	return (!canmove(LEFT) && !canmove(RIGHT)
			&& !canmove(UP) && !canmove(DOWN));
}

bool chkwin()
{
	int x, y;
	BOARD_FOREACH(x, y)
		if (board[x][y] == 2048)
			return true;

	return false;
}

void exithdl()
{
	endwin();
}

void win()
{
	clear();
	printw("You won!\n\n[Press enter to exit]");
	refresh();
	while (getch() != '\n');

	exit(0);
}

void lose()
{
	clear();
	printw("You lost!\n\n[Press enter to exit]");
	refresh();
	while (getch() != '\n');

	exit(0);
}


// get user input from curses and act on it
bool userinput()
{
	int ch = getch();

	switch(ch) {
		KEYTODIR(KEY_LEFT, 'h', LEFT);
		KEYTODIR(KEY_RIGHT, 'l', RIGHT);
		KEYTODIR(KEY_UP, 'k', UP);
		KEYTODIR(KEY_DOWN, 'j', DOWN);
		case 'q':
			exit(0);
			break;
		case KEY_RESIZE: // if a SIGWINCH has been caught
			chksize();
			break;
		default: // invalid input, ignore
			return false;
			break;
	}

	return true;
}

// get color from block value
int getcolor(short block)
{
	switch(block) {
		case 0:    return A_REVERSE | A_DIM;            break;
		case 2:    return COLOR_PAIR(RED);              break;
		case 4:    return COLOR_PAIR(GREEN);            break;
		case 8:    return COLOR_PAIR(YELLOW);           break;
		case 16:   return COLOR_PAIR(BLUE);             break;
		case 32:   return COLOR_PAIR(MAGENTA);          break;
		case 64:   return COLOR_PAIR(CYAN);             break;
		case 128:  return COLOR_PAIR(RED)     | A_BOLD; break;
		case 256:  return COLOR_PAIR(GREEN)   | A_BOLD; break;
		case 512:  return COLOR_PAIR(YELLOW)  | A_BOLD; break;
		case 1024: return COLOR_PAIR(BLUE)    | A_BOLD; break;
		case 2048: return COLOR_PAIR(MAGENTA) | A_BOLD; break;
		default: // this shouldn't ever happen
			endwin();
			fprintf(stderr, "2048: getcolor: invalid block number");
			exit(1);
			break;
	}
}

// checks for sufficient screen size, otherwise exits
void chksize()
{
	int wrow, wcol;

	getmaxyx(stdscr, wrow, wcol);
	if (wrow < (SIZE * 3) + 2 || wcol < SIZE * 7) {
		endwin();
		fprintf(stderr, "2048: terminal must be at least %ix%i\n",
				SIZE * 7, (SIZE * 3) + 2);
		exit(1);
	}
}

// initialize colors for ncurses
void initcolor()
{
	if (has_colors() == FALSE) {
		endwin();
		fprintf(stderr, "2048: needs colors\n");
		exit(1);
	}

	start_color();
	use_default_colors();

	init_pair(RED, -1, COLOR_RED);
	init_pair(GREEN, -1, COLOR_GREEN);
	init_pair(YELLOW, -1, COLOR_YELLOW);
	init_pair(BLUE, -1, COLOR_BLUE);
	init_pair(MAGENTA, -1, COLOR_MAGENTA);
	init_pair(CYAN, -1, COLOR_CYAN);
}

int main(int argc, char *argv[])
{
	int x, y, t, color;
	srand(time(NULL));

	initscr();
	clear();
	noecho();
	cbreak();
	keypad(stdscr, TRUE);
	notimeout(stdscr, TRUE);
	curs_set(0);

	atexit(&exithdl);

	initcolor();

	populate();

	// main loop
	for (;;) {
		// reset merge board
		memset(&merges, 0, sizeof(char) * (SIZE * SIZE));

		chksize();
		populate();
		if (chklose())
			lose();

		if (chkwin())
			win();

		mvprintw(0, 0, "%s", getlogin());
		mvprintw(0, ((SIZE * 7) - (cntdigits(score) + 7)),
				"score: %lu", score);

		BOARD_FOREACH(x, y) {
			color = getcolor(board[y][x]);
			t = 7 - cntdigits(board[y][x]);

			attron(color);
			mvprintw((y * 3) + 2, x * 7, "       ");
			if (board[y][x] == 0) {
				mvprintw((y * 3) + 3, x * 7, "%*s%c%*s",
						t - t / 2, "",
						'-',
						t / 2, "");
			} else {
				mvprintw((y * 3) + 3, x * 7, "%*s%d%*s",
						t - t / 2, "",
						board[y][x],
						t / 2, "");
			}
			mvprintw((y * 3) + 4, x * 7, "       ");
			attroff(color);
		}

		refresh();
		while (!userinput());
		clear();
	}

	return 0;
}
